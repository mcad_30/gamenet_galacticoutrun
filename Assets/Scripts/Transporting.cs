﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class Transporting : NetworkBehaviour {

	public Collider Prison;
	public Collider RunnersArea;
	public Collider DefendersArea;

	[Command]
	public void CmdPrisonTransport(GameObject player){
		RpcTransportToPrison (player);
	}
		
	[ClientRpc]
	public void RpcTransportToPrison(GameObject player) {

		Vector2 newPos = new Vector2 (Random.Range (Prison.bounds.min.x, Prison.bounds.max.x), Random.Range (Prison.bounds.min.y, Prison.bounds.max.y));
		player.transform.position = newPos;
	}
	[ClientRpc]
	public void RpcTransportToRunnerBase (GameObject player){
		Vector2 newPos = new Vector2 (Random.Range (RunnersArea.bounds.min.x, RunnersArea.bounds.max.x), Random.Range (RunnersArea.bounds.min.y, RunnersArea.bounds.max.y));
		player.transform.position = newPos;
		player.tag = "Runner";
		player.layer = 8;
		player.gameObject.GetComponent<Renderer> ().material.color = Color.red;
	}

	[ClientRpc]
	public void RpcTransportToDefenderBase (GameObject player){
		Vector2 newPos = new Vector2 (Random.Range (DefendersArea.bounds.min.x, DefendersArea.bounds.max.x), Random.Range (DefendersArea.bounds.min.y, DefendersArea.bounds.max.y));
		player.transform.position = newPos;
		player.tag = "Defender";
		player.layer = 9;
		player.gameObject.GetComponent<Renderer> ().material.color = Color.blue;
	}

}