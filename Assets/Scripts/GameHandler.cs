﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.Networking;

public class GameHandler : NetworkBehaviour
{
    [SyncVar(hook = "OnChangeTeamAScore")]
    public float TeamAScore;

    [SyncVar(hook = "OnChangeTeamBScore")]
    public float TeamBScore;

    [SyncVar(hook = "OnChangeTimer")]
	public float MaxTime = 1;

	public GameObject TeamAWin;
	public GameObject TeamBWin;
    public GameObject Draw;

    public Text TeamAScoreText;
    public Text TeamBScoreText;

	public Text TimerText;

    void Start()
    {
        TeamAScoreText.text = TeamAScore.ToString();
        TeamBScoreText.text = TeamBScore.ToString();
        MaxTime *= 60;
    }

    // Update is called once per frame
    void Update()
    {
        TeamAScoreText.text = TeamAScore.ToString();
        TeamBScoreText.text = TeamBScore.ToString();
        // Game Timer
       	if (NetworkServer.active)
        {
			MaxTime -= Time.deltaTime;

        }

		if (MaxTime <= 0)
        {
			MaxTime = 0;
            // Team A Wins
            if (TeamAScore > TeamBScore)
            {
				RpcTeamAWin ();
            }
            // Team B Wins
            else if (TeamBScore > TeamAScore)
            {
				RpcTeamBWin ();
            }
            // Draw
            else if (TeamBScore == TeamAScore)
            {
				RpcDraw ();

            }
        }
    }

	[ClientRpc]
	void RpcTeamAWin(){
		TeamAWin.SetActive (true);
	}

	[ClientRpc]
	void RpcTeamBWin(){
		TeamBWin.SetActive (true);
	}

	[ClientRpc]
	void RpcDraw(){
		Draw.SetActive (true);
	}

	void OnChangeTimer(float MaxTime)
    {
		float min = Mathf.Floor (MaxTime / 60);
		float sec = MaxTime % 60;

		TimerText.text = min.ToString () + ":" + Mathf.RoundToInt (sec).ToString ();
    }

    void OnChangeTeamAScore(float teamAScore)
    {
        TeamAScore = teamAScore;
    }

    void OnChangeTeamBScore(float teamBScore)
    {
        TeamBScore = teamBScore;
    }
}
