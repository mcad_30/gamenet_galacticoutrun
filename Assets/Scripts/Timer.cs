﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Timer : MonoBehaviour {

    public Text TimerTxt;
    public int Minutes;

    private System.TimeSpan timeSpan;
	
    void Start()
    {
        timeSpan = new System.TimeSpan(0, Minutes, 0);
    }

	void Update () {
        timeSpan -= System.TimeSpan.FromSeconds(Time.deltaTime);
        UpdateText();
	}

    void UpdateText()
    {
        string time = String.Format("{0:0}:{1:00}", timeSpan.Minutes, timeSpan.Seconds);
        TimerTxt.text = time;
    }
}
