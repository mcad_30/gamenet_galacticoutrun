﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orb : MonoBehaviour
{

    public GameHandler GameHandler;

    Renderer orgColor;
    Renderer curColor;

    private void Start()
    {
        orgColor = GetComponent<Renderer>();
        orgColor.material.color = this.GetComponent<Renderer>().material.color;
    }
    private void Update()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
    }

    // If this game object (orb) is inside a team's base.
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "TeamA")
        {
            Debug.Log("SCOREEEE");
            // If this orb is a big orb
            if (gameObject.tag == "Big Orb")
            {
                this.gameObject.GetComponent<Renderer>().material.color = Color.blue;
                GameHandler.TeamAScore += 1.5f;
            }
            else GameHandler.TeamAScore += 0.5f;
        }

        if (other.gameObject.tag == "TeamB")
        {
            Debug.Log("SCOREEEE");
            // If this orb is a big orb
            if (gameObject.tag == "Big Orb")
            {
                this.gameObject.GetComponent<Renderer>().material.color = Color.red;
                GameHandler.TeamBScore += 1.5f;
            }
            else GameHandler.TeamBScore += 0.5f;

        }
    }

    // If this game object (orb) goes outside a team's base.
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "TeamA")
        {
            // If this orb is a big orb
            if (gameObject.tag == "Big Orb")
            {
                
                this.gameObject.GetComponent<Renderer>().material.color = Color.blue;
                GameHandler.TeamAScore -= 1.5f;
            }
            else GameHandler.TeamAScore -= 0.5f;

        }

        if (other.gameObject.tag == "TeamB")
        {
            // If this orb is a big orb
            if (gameObject.tag == "Big Orb")
            {
                this.gameObject.GetComponent<Renderer>().material.color = Color.red;
                GameHandler.TeamBScore -= 1.5f;
            }
            else GameHandler.TeamBScore -= 0.5f;
        }

        this.GetComponent<Renderer>().material.color = orgColor.material.color;
    }
}
