﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Movement : NetworkBehaviour
{
    public float speed = 1.0f;
    public bool canCatch = false;
    public bool isInPrison;

    private Collision collision;
    public NetworkStartPosition[] spawnPoints;
    // Use this for initialization
    void Start()
    {
        spawnPoints = FindObjectsOfType<NetworkStartPosition>();
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

        if (!isLocalPlayer)
        {
            return;
        }
        Vector3 movement = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
            movement.y += speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.A))
            movement.x -= speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.S))
            movement.y -= speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.D))
            movement.x += speed * Time.deltaTime;

        this.transform.Translate(movement.x, movement.y, 0);
    }
}
